import React, { Component } from 'react';
import './home.css'
import { FaBuilding, FaHandsHelping, FaChartPie, FaTags, FaBars } from 'react-icons/fa'
import { Link } from 'react-router-dom'
import { Outlet } from 'react-router-dom'

class Home extends Component {

    handleClick() {
        const $wrapper = document.querySelector('#wrapper');
        $wrapper.classList.toggle("toggled");
    };

    componentDidMount() {
        const menu_items = document.querySelectorAll('.sidebar-nav > li');
        menu_items.forEach(mi => {
            mi.addEventListener('click', (e) => {
                menu_items.forEach(mi => {
                    mi.classList.remove("active")
                })
                mi.classList.add("active");
            });
        });
    }

    render() {
        return (
            <>
                <div id="wrapper">
                    <aside id="sidebar-wrapper">
                        <div className="sidebar-brand">
                            <h2>Logo</h2>
                        </div>
                        <ul className="sidebar-nav">
                            <li className="">
                                <Link to="/home">
                                    <a id="links-menu"><i className='fa'><FaChartPie /></i> Dashboard</a>
                                </Link>
                            </li>
                            <li>
                                <Link to="/home/parceria">
                                    <a id="links-menu"><i className='fa'><FaHandsHelping /></i> Parceiros</a>
                                </Link>
                            </li>
                            <li>
                                <Link to="/home/empresa">
                                    <a id="links-menu"><i className='fa'><FaBuilding /></i> Empresas</a>
                                </Link>
                            </li>
                            <li>
                                <Link to="/home/cupons">
                                    <a id="links-menu"><i className='fa'><FaTags /></i> Cupons</a>
                                </Link>
                            </li>
                        </ul>
                    </aside>

                    <div id="navbar-wrapper">
                        <nav className="navbar navbar-inverse">
                            <div className="container-fluid">
                                <div className="navbar-header" onClick={this.handleClick}>
                                    <a href="#" className="navbar-brand" id="sidebar-toggle"><FaBars /></a>
                                </div>
                            </div>
                        </nav>
                    </div>

                    <section id="content-wrapper">
                        <div className="row" >
                            <div className="col-lg-12 d-flex justify-content-center align-items-center">
                                <Outlet />
                            </div>
                        </div>
                    </section>

                </div>
            </>
        );
    }
}
export default Home;