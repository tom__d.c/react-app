import './Table.css';
import React, { Component } from 'react';
const $ = require('jquery');
$.DataTable = require('datatables.net')


class Table extends Component {
    componentDidMount(){
        this.$el = $(this.el)
        this.$el.DataTable({
            data: this.props.data,
            columns: this.props.fields,
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.4/i18n/pt-BR.json'
            }
        });
    }

    componentWillUnmount(){

    }

    render(){
        return (
            <div>
                <table className="display" width="100%" ref={el => this.el = el}></table>
            </div>
        )
    }
}

export default Table;