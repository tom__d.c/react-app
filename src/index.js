import React from 'react';
import ReactDOM from 'react-dom/client';
import Login from './routes/Login/login';
import Recover from './routes/Recover/recover';
import ResetPass from './routes/ResetPass/resetPass';
import Dashboard from './routes/Dashboard/dashboard';
import Parceria from './routes/Parceiro/Parceiro';
import Home from './components/Home/Home';
import Empresa from './routes/Empresas/Empresa';
import Cupom from './routes/Cupons/Cupom';

import { createBrowserRouter, RouterProvider } from 'react-router-dom'
// Bootstrap CSS
import "bootstrap/dist/css/bootstrap.min.css";
// Bootstrap Bundle JS
import "bootstrap/dist/js/bootstrap.bundle.min";
import './index.css';

const router = createBrowserRouter([
  {
    path: "/",
    element: <Login />
  },
  {
    path: "/reset_password",
    element: <ResetPass />
  },
  {
    path: "/recover",
    element: <Recover />
  },
  {
    path: "/home",
    element: <Home />,
    children: [
      {
        path: "dashboard",
        element: <Dashboard />
      },
      {
        path: "parceria",
        element: <Parceria />
      },
      {
        path: "empresa",
        element: <Empresa />
      },
      {
        path: 'cupons',
        element: <Cupom />
      }
    ]
  },

])

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <RouterProvider router={router} />
);
