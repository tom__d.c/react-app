import React, { Component } from 'react';
import { FaLock } from 'react-icons/fa'
import styles from './resetPass.module.css'

class ResetPass extends Component {
    render() {
        return (
            <div className='container-fluid'>
                <div className='h-100 d-flex align-items-center justify-content-center'>
                    <div style={{ top: "25%", position: 'absolute', width: '90%', paddingLeft: '100px'}}>
                        <form>
                            <div className='row' style={{ justifyContent: 'center' }} >
                                <div className={styles.alc + ' col-md-4'} >
                                    <div style={{ padding: '50px', width: '100%' }}>

                                        LOGO
                                        <br /><br />
                                        <h2>Insira a nova senha e confirme novamente</h2>
                                        <br />
                                        <div className='col-auto' >
                                            <div className="input-group mb-2">
                                                <div className="input-group-text"><FaLock /></div>

                                                <input type="password" className="form-control" id="inlineFormInputGroup" placeholder="Sua nova senha" />
                                            </div>
                                        </div>
                                        <div className='col-auto' >
                                            <div className="input-group mb-2">
                                                <div className="input-group-text"><FaLock /></div>

                                                <input type="password" className="form-control" id="inlineFormInputGroup" placeholder="Sua nova senha" />
                                            </div>
                                        </div>
                                        <br />
                                        <button className='btn btn-primary btn-lg btn-block' size="lg" style={{ width: '100%' }}>Confirmar</button>
                                    </div>
                                </div>
                                <div className='col-md-4' style={{ backgroundColor: "#065AD8", borderBottomRightRadius: '12px', borderTopRightRadius: '12px' }}></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default ResetPass;