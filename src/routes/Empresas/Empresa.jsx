import React, { Component } from 'react';
import style from './empresa.module.css';
import 'bootstrap-select';
import Table from '../../components/Table/Table';
import $ from 'jquery';
import Select from 'react-select'
import 'jquery-mask-plugin/dist/jquery.mask.min';

class Empresa extends Component {

    componentDidMount() {
        $('#cnpj').mask("99.999.999.9999/99")
        $('#telefone').mask("(99) 99999-9999")
        $("#cep").mask("99999-999")
    }

    render() {
        const options = [
            { value: 'chocolate', label: 'Chocolate' },
            { value: 'strawberry', label: 'Strawberry' },
            { value: 'vanilla', label: 'Vanilla' }
        ]
        return (
            <>
                <div className="col-11 mt-5 ms-3">


                    <button type="button" className="btn btn-primary mb-2" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        Cadastro Empresa
                    </button>


                    <div className="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h1 className="modal-title fs-5 " id="exampleModalLabel">Cadastro empresa</h1>
                                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div className="modal-body">

                                    <div className="row justify-content-md-center" >
                                        <div className="col-md-12" style={{ backgroundColor: '#fff', borderRadius: '7px' }}>
                                            <ul className="list-group list-group-flush">
                                                <li className="list-group-item">
                                                    <ul className="nav nav-tabs" id="myTab" role="tablist">
                                                        <li className="nav-item" role="presentation">
                                                            <button className="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">Empresa</button>
                                                        </li>
                                                        <li className="nav-item" role="presentation">
                                                            <button className="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false">Endereço</button>
                                                        </li>
                                                        <li className="nav-item" role="presentation">
                                                            <button className="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact-tab-pane" type="button" role="tab" aria-controls="contact-tab-pane" aria-selected="false">Usuário</button>
                                                        </li>
                                                    </ul>
                                                    <div className="tab-content" id="myTabContent" style={{ padding: '20px' }}>
                                                        <div className="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                                                            <form action="">
                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Nome Fantasia</label>
                                                                    <input type="text" className="form-control" placeholder="Seu Nome Fantasia" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Parceria:</label>
                                                                    <Select options={options} isMulti />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Cupons:</label>
                                                                    <Select options={options} isMulti />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Razão Social</label>
                                                                    <input type="text" className="form-control" placeholder="Sua Razão Social" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">CNPJ:</label>
                                                                    <input type="text" id="cnpj" className="form-control" placeholder="CNPJ" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Telefone:</label>
                                                                    <input type="text" id="telefone" className="form-control" placeholder="Seu Telefone" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">E-mail:</label>
                                                                    <input type="email" className="form-control" placeholder="Seu e-mail" />
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div className="tab-pane fade" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
                                                            <form action="">
                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">CEP:</label>
                                                                    <input type="text" id="cep" className="form-control" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Logradouro:</label>
                                                                    <input type="text" className="form-control" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Número:</label>
                                                                    <input type="text" className="form-control" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Bairro:</label>
                                                                    <input type="text" className="form-control" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Cidade:</label>
                                                                    <input type="text" className="form-control" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Estado:</label>
                                                                    <input type="text" className="form-control" />
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div className="tab-pane fade" id="contact-tab-pane" role="tabpanel" aria-labelledby="contact-tab" tabindex="0">
                                                            <form action="">
                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Login:</label>
                                                                    <input type="text" className="form-control" placeholder="Seu login" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Senha:</label>
                                                                    <input type="password" className="form-control" placeholder="Sua Senha" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Confirmar Senha:</label>
                                                                    <input type="password" className="form-control" placeholder="Sua Senha" />
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div >

                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                                    {/*<button type="button" className="btn btn-primary">Save changes</button>*/}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={style.tableContent}>
                        <div className="table-responsive-sm">
                            <Table data={this.dataSet} fields={this.fieldSet} />
                        </div>
                    </div>


                </div>
            </>
        )
    }

    fieldSet = [
        {title: 'Nome Fantasia'},
        {title: 'Parceiria'},
        {title: 'Razão Social'},
        {title: 'CNPJ'},
        {title: 'Telefone'},
        {title: 'E-mail'}
    ]

    dataSet = [
        ['Tiger Nixon', 'System Architect', 'Edinburgh', '5421', '2011/04/25', '$320,800'],
        ['Garrett Winters', 'Accountant', 'Tokyo', '8422', '2011/07/25', '$170,750'],
        ['Ashton Cox', 'Junior Technical Author', 'San Francisco', '1562', '2009/01/12', '$86,000'],
        ['Cedric Kelly', 'Senior Javascript Developer', 'Edinburgh', '6224', '2012/03/29', '$433,060'],
        ['Airi Satou', 'Accountant', 'Tokyo', '5407', '2008/11/28', '$162,700'],
        ['Brielle Williamson', 'Integration Specialist', 'New York', '4804', '2012/12/02', '$372,000'],
        ['Herrod Chandler', 'Sales Assistant', 'San Francisco', '9608', '2012/08/06', '$137,500'],
        ['Rhona Davidson', 'Integration Specialist', 'Tokyo', '6200', '2010/10/14', '$327,900'],
        ['Colleen Hurst', 'Javascript Developer', 'San Francisco', '2360', '2009/09/15', '$205,500'],
        ['Sonya Frost', 'Software Engineer', 'Edinburgh', '1667', '2008/12/13', '$103,600'],
        ['Jena Gaines', 'Office Manager', 'London', '3814', '2008/12/19', '$90,560'],
        ['Quinn Flynn', 'Support Lead', 'Edinburgh', '9497', '2013/03/03', '$342,000'],
        ['Charde Marshall', 'Regional Director', 'San Francisco', '6741', '2008/10/16', '$470,600'],
        ['Haley Kennedy', 'Senior Marketing Designer', 'London', '3597', '2012/12/18', '$313,500'],
        ['Tatyana Fitzpatrick', 'Regional Director', 'London', '1965', '2010/03/17', '$385,750'],
        ['Michael Silva', 'Marketing Designer', 'London', '1581', '2012/11/27', '$198,500'],
        ['Paul Byrd', 'Chief Financial Officer (CFO)', 'New York', '3059', '2010/06/09', '$725,000'],
        ['Gloria Little', 'Systems Administrator', 'New York', '1721', '2009/04/10', '$237,500'],
        ['Bradley Greer', 'Software Engineer', 'London', '2558', '2012/10/13', '$132,000'],
        ['Dai Rios', 'Personnel Lead', 'Edinburgh', '2290', '2012/09/26', '$217,500'],
        ['Jenette Caldwell', 'Development Lead', 'New York', '1937', '2011/09/03', '$345,000'],
        ['Yuri Berry', 'Chief Marketing Officer (CMO)', 'New York', '6154', '2009/06/25', '$675,000'],
        ['Caesar Vance', 'Pre-Sales Support', 'New York', '8330', '2011/12/12', '$106,450'],
        ['Doris Wilder', 'Sales Assistant', 'Sydney', '3023', '2010/09/20', '$85,600'],
        ['Angelica Ramos', 'Chief Executive Officer (CEO)', 'London', '5797', '2009/10/09', '$1,200,000'],
        ['Gavin Joyce', 'Developer', 'Edinburgh', '8822', '2010/12/22', '$92,575'],
        ['Jennifer Chang', 'Regional Director', 'Singapore', '9239', '2010/11/14', '$357,650'],
        ['Brenden Wagner', 'Software Engineer', 'San Francisco', '1314', '2011/06/07', '$206,850'],
        ['Fiona Green', 'Chief Operating Officer (COO)', 'San Francisco', '2947', '2010/03/11', '$850,000'],
        ['Shou Itou', 'Regional Marketing', 'Tokyo', '8899', '2011/08/14', '$163,000'],
        ['Michelle House', 'Integration Specialist', 'Sydney', '2769', '2011/06/02', '$95,400'],
        ['Suki Burks', 'Developer', 'London', '6832', '2009/10/22', '$114,500'],
        ['Prescott Bartlett', 'Technical Author', 'London', '3606', '2011/05/07', '$145,000'],
        ['Gavin Cortez', 'Team Leader', 'San Francisco', '2860', '2008/10/26', '$235,500'],
        ['Martena Mccray', 'Post-Sales support', 'Edinburgh', '8240', '2011/03/09', '$324,050'],
        ['Unity Butler', 'Marketing Designer', 'San Francisco', '5384', '2009/12/09', '$85,675'],
    ];
}

export default Empresa;