import React, { Component } from 'react';
import style from './parceiro.module.css'
import 'bootstrap-select'
import Table from '../../components/Table/Table';
import $ from 'jquery';
import 'jquery-mask-plugin/dist/jquery.mask.min';

class Parceria extends Component {

    componentDidMount() {
        $('#cnpj').mask("99.999.999.9999/99")
        $('#telefone').mask("(99) 99999-9999")
        $("#cep").mask("99999-999")
    }

    render() {

        return (
            <>
                <div className="col-11 mt-5 ms-3">


                    <button type="button" className="btn btn-primary mb-2" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        Cadastro Parceiros
                    </button>


                    <div className="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h1 className="modal-title fs-5 " id="exampleModalLabel">Cadastro parceiros</h1>
                                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div className="modal-body">

                                    <div className="row justify-content-md-center" >
                                        <div className="col-md-12" style={{ backgroundColor: '#fff', borderRadius: '7px' }}>
                                            <ul className="list-group list-group-flush">
                                                <li className="list-group-item">
                                                    <ul className="nav nav-tabs" id="myTab" role="tablist">
                                                        <li className="nav-item" role="presentation">
                                                            <button className="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">Parceria</button>
                                                        </li>
                                                        <li className="nav-item" role="presentation">
                                                            <button className="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false">Endereço</button>
                                                        </li>
                                                        <li className="nav-item" role="presentation">
                                                            <button className="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact-tab-pane" type="button" role="tab" aria-controls="contact-tab-pane" aria-selected="false">Usuário</button>
                                                        </li>
                                                    </ul>
                                                    <div className="tab-content" id="myTabContent" style={{ padding: '20px' }}>
                                                        <div className="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                                                            <form action="">

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Razão Social</label>
                                                                    <input type="text" className="form-control" placeholder="Sua Razão Social" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">CNPJ:</label>
                                                                    <input type="text" id="cnpj" className="form-control" placeholder="CNPJ" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Telefone:</label>
                                                                    <input type="text" id="telefone" className="form-control" placeholder="Seu Telefone" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">E-mail:</label>
                                                                    <input type="email" className="form-control" placeholder="Seu e-mail" />
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div className="tab-pane fade" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
                                                            <form action="">
                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">CEP:</label>
                                                                    <input type="text" id="cep" className="form-control" />
                                                                </div>
                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Logradouro:</label>
                                                                    <input type="text" className="form-control" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Número:</label>
                                                                    <input type="text" className="form-control" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Bairro:</label>
                                                                    <input type="text" className="form-control" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Cidade:</label>
                                                                    <input type="text" className="form-control" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Estado:</label>
                                                                    <input type="text" className="form-control" />
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div className="tab-pane fade" id="contact-tab-pane" role="tabpanel" aria-labelledby="contact-tab" tabindex="0">
                                                            <form action="">
                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Login:</label>
                                                                    <input type="text" className="form-control" placeholder="Seu login" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Senha:</label>
                                                                    <input type="password" className="form-control" placeholder="Sua Senha" />
                                                                </div>

                                                                <div className="mb-3">
                                                                    <label for="exampleFormControlInput1" className="form-label">Confirmar Senha:</label>
                                                                    <input type="password" className="form-control" placeholder="Sua Senha" />
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div >

                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                                    {/*<button type="button" className="btn btn-primary">Save changes</button>*/}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={style.tableContent}>
                        <div className="table-responsive-sm">
                            <Table data={this.dataSet} fields={this.fieldSet} />
                        </div>
                    </div>
                </div>
            </>
        )
    }

    fieldSet = [
        {title: 'Razão Social'},
        {title: 'CNPJ'},
        {title: 'Telefone'},
        {title: 'E-mail'}
    ]

    dataSet = [
        ['Tiger Nixon', 'System Architect', 'Edinburgh', '5421'],
        ['Garrett Winters', 'Accountant', 'Tokyo', '8422'],
        ['Ashton Cox', 'Junior Technical Author', 'San Francisco', '1562'],
        ['Cedric Kelly', 'Senior Javascript Developer', 'Edinburgh', '6224'],
        ['Airi Satou', 'Accountant', 'Tokyo', '5407'],
        ['Brielle Williamson', 'Integration Specialist', 'New York', '4804'],
        ['Herrod Chandler', 'Sales Assistant', 'San Francisco', '9608'],
        ['Rhona Davidson', 'Integration Specialist', 'Tokyo', '6200'],
        ['Colleen Hurst', 'Javascript Developer', 'San Francisco', '2360'],
        ['Sonya Frost', 'Software Engineer', 'Edinburgh', '1667'],
        ['Jena Gaines', 'Office Manager', 'London', '3814'],
        ['Quinn Flynn', 'Support Lead', 'Edinburgh', '9497'],
        ['Charde Marshall', 'Regional Director', 'San Francisco', '6741'],
        ['Haley Kennedy', 'Senior Marketing Designer', 'London', '3597'],
        ['Tatyana Fitzpatrick', 'Regional Director', 'London', '1965'],
        ['Michael Silva', 'Marketing Designer', 'London', '1581'],
        ['Paul Byrd', 'Chief Financial Officer (CFO)', 'New York', '3059'],
        ['Gloria Little', 'Systems Administrator', 'New York', '1721'],
        ['Bradley Greer', 'Software Engineer', 'London', '2558'],
        ['Dai Rios', 'Personnel Lead', 'Edinburgh', '2290'],
        ['Jenette Caldwell', 'Development Lead', 'New York', '1937'],
        ['Yuri Berry', 'Chief Marketing Officer (CMO)', 'New York', '6154'],
        ['Caesar Vance', 'Pre-Sales Support', 'New York', '8330'],
        ['Doris Wilder', 'Sales Assistant', 'Sydney', '3023'],
        ['Angelica Ramos', 'Chief Executive Officer (CEO)', 'London', '5797'],
        ['Gavin Joyce', 'Developer', 'Edinburgh', '8822'],
        ['Jennifer Chang', 'Regional Director', 'Singapore', '9239'],
        ['Brenden Wagner', 'Software Engineer', 'San Francisco', '1314'],
        ['Fiona Green', 'Chief Operating Officer (COO)', 'San Francisco', '2947'],
        ['Shou Itou', 'Regional Marketing', 'Tokyo', '8899'],
        ['Michelle House', 'Integration Specialist', 'Sydney', '2769'],
        ['Suki Burks', 'Developer', 'London', '6832'],
        ['Prescott Bartlett', 'Technical Author', 'London', '3606'],
        ['Gavin Cortez', 'Team Leader', 'San Francisco', '2860'],
        ['Martena Mccray', 'Post-Sales support', 'Edinburgh', '8240'],
        ['Unity Butler', 'Marketing Designer', 'San Francisco', '5384'],
    ];
}

export default Parceria;