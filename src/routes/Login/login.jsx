import React, { Component } from 'react';
import { FaLock, FaRegEnvelope, FaGoogle, FaFacebook } from 'react-icons/fa'
import styles from './login.module.css'

class Login extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div className='h-100 d-flex align-items-center justify-content-center'>
                    <div style={{ top: "25%", position: 'absolute', width: '90%', marginLeft: '50px' }}>
                        <form>
                            <div className='row' style={{ justifyContent: 'center' }} >
                                <div className={styles.alc + ' col-md-4 '} >
                                    <div style={{ padding: '50px', width: '100%' }}>

                                        LOGO
                                        <br /><br />
                                        <h2>Faça login em sua conta</h2>
                                        <br />

                                        <div className='col-auto' >
                                            <div className="input-group mb-2">
                                                <div className="input-group-text"><FaRegEnvelope /></div>

                                                <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Seu e-mail" />
                                            </div>
                                        </div>
                                        <div className='col-auto' >
                                            <div className="input-group mb-2">
                                                <div className="input-group-text"><FaLock /></div>

                                                <input type="password" className="form-control" id="inlineFormInputGroup" placeholder="Sua senha secreta" />
                                            </div>
                                        </div>
                                        <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <div>
                                                <input className="form-check-input" // prettier-ignore
                                                    type="checkbox"
                                                    id="default-checkbox"
                                                    label="Continuar conectado"
                                                />
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    &nbsp; Continuar conectado
                                                </label>
                                            </div>
                                            <a>Esqueceu sua senha?</a>
                                        </div>
                                        <br />
                                        <button className='btn btn-primary btn-lg btn-block' size="lg" style={{ width: '100%' }}>Logar</button>
                                        <br /><br />
                                        <center><p>Você não possui uma conta? Crie uma agora!</p></center>
                                    </div>
                                </div>
                                <div className='col-md-4' style={{ backgroundColor: "#065AD8", borderBottomRightRadius: '12px', borderTopRightRadius: '12px' }}></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div >
        )
    }
}

export default Login;