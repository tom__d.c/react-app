import React, { Component } from 'react';
import { FaRegEnvelope } from 'react-icons/fa'
import styles from './recover.module.css'

class Recover extends Component {
    render() {
        return (
            <div className='containe-fluid'>
                <div className='h-100 d-flex align-items-center justify-content-center'>
                    <div style={{ top: "25%", position: 'absolute', width: '90%', }}>
                        <form>
                            <div className='row' style={{ justifyContent: 'center' }} >
                                <div className={styles.alc + ' col-md-4'}>
                                    <div style={{ padding: '50px', width: '100%' }}>
                                        <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                                            <div>
                                                LOGO
                                            </div>
                                            <a href="/" className='text-decoration-none'>Voltar</a>
                                        </div>
                                        <br />
                                                <h2>Insira o email abaixo para recuperar a conta</h2>
                                        <br />

                                        <div className='col-auto' >
                                            <div className="input-group mb-2">
                                                <div className="input-group-text"><FaRegEnvelope /></div>

                                                <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Seu e-mail" />
                                            </div>
                                        </div>

                                        <br />
                                        <button className='btn btn-primary btn-lg btn-block' size="lg" style={{ width: '100%' }}>Enviar</button>
                                    </div>
                                </div>
                                <div className='col-md-4 col-md-12' style={{ backgroundColor: "#065AD8", borderBottomRightRadius: '12px', borderTopRightRadius: '12px' }}></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Recover;